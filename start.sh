#!/bin/bash
set -e
source variables.txt
export $(cut -d= -f1 variables.txt)
echo "¿Que acción desea realizar? (CREATE, DESTROY)"
read resp
if [ "$resp" == "CREATE" ] 
then
if [ ! -f ./my-key ]
then
echo "Llave ssh no encontrada. Se procede a crear"
ssh-keygen -t rsa -f my-key -C my-user -N ''
fi
chmod 400 ./my-key
terraform init ./TerraformGCE
terraform apply -auto-approve ./TerraformGCE
elif [ "$resp" == "DESTROY" ]
then
terraform destroy -auto-approve ./TerraformGCE
else
echo "Las únicas respuestas aceptadas son CREATE o DESTROY"
fi
echo "Fin"
