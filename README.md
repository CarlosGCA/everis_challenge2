# Everis_Challenge2
Los prerequisitos para iniciar el proyecto es tener instalados correctamente los siguientes comandos en el entorno ejecutor:
- terraform (<=v0.13.2)
- ssh-keygen

SOLO PARA LINUX CENTOS: Es posible instalar terraform ejecutando el archivo installDependencies.sh
En el caso de Windows adicionalmente es necesario contar con Git Bash para ejecutar el script del proyecto.

Una vez se cuente con los prerequisitos, los siguientes pasos obligatorios son:
1. Crear un Service Account, bajar las credenciales en formato json y copiar el archivo en la carpeta del proyecto (misma carpeta que el presente README.md).
2. Llenar el archivo variables.txt con las datos correspondientes de nombre de proyecto, región, zona y nombre del archivo de credenciales json que fue incluido en esta carpeta en el paso anterior. Es importante no dejar ningun espacio entre el '=' y el fin de linea.

Finalmente, ejecutar el archivo start.sh como administrador o root. 

Luego de la creación de la infraestructura se mostrará como output la url al Jenkins creado.

# Información del proyecto
/TerraformGCE : Carpeta con los archivos tf que generan la infraestructura para el GCE
/Jenkins-installation : Carpeta con proyecto yml que configura el GCE con Jenkins además de instalar Maven y Git. Para lograr ejecutar este proyecto con Ansible en la instancia, se genera una llave ssh durante la ejecución del start.sh