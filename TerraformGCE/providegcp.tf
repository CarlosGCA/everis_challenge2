provider "google" {
  version = "3.5.0"

  credentials = file("${var.credentials_file}")

  project = var.project
  region  = var.region
  zone    = var.zone
}

resource "google_compute_instance" "my_instance" {
  name = "my-instance"
  machine_type = "e2-small"

  boot_disk {
    initialize_params {
      image = "centos-cloud/centos-8"
    }
  }

  network_interface {
    network = google_compute_network.my_vpc-network.self_link

    access_config {
      // Ephemeral IP
    }
  }

  metadata = {
    ssh-keys = "my-user:${file("my-key.pub")}"
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir Jenkins-installation",
      "sudo dnf makecache",
      "sudo dnf install epel-release -y",
      "sudo dnf makecache",
      "sudo dnf install ansible -y"  
    ]

    connection {
      type = "ssh"
      user = "my-user"
      private_key = "${file("my-key")}"
      agent = "false"
      host = self.network_interface.0.access_config.0.nat_ip
    }
  }

  provisioner "file" {
    source = "./Jenkins-installation/"
    destination = "/home/my-user/Jenkins-installation"

    connection {
      type = "ssh"
      user = "my-user"
      private_key = "${file("my-key")}"
      agent = "false"
      host = self.network_interface.0.access_config.0.nat_ip
    }
  }

  provisioner "remote-exec" {
    inline = [
      "sudo ansible-playbook /home/my-user/Jenkins-installation/site.yml",
    ]

    connection {
      type = "ssh"
      user = "my-user"
      private_key = "${file("my-key")}"
      agent = "false"
      host = self.network_interface.0.access_config.0.nat_ip
    }
  }
}

resource "google_compute_network" "my_vpc-network" {
  name = "terraform-network"
  auto_create_subnetworks = "true"
}

resource "google_compute_firewall" "my_firewall" {
  name    = "my-firewall"
  network = google_compute_network.my_vpc-network.name

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["22", "8080", "5000"]
  }
}

output "jenkins-url" {
  value = "${google_compute_instance.my_instance.network_interface.0.access_config.0.nat_ip}:8080"
}